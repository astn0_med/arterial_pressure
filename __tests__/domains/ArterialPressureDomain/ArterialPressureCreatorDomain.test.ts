import ArterialPressureCreatorDomain from '../../../src/domains/ArterialPressureDomain/ArterialPressureCreatorDomain';
import IArterialPressureCreatorDomain from '../../../src/domains/ArterialPressureDomain/IArterialPressureCreatorDomain';
import IArterialPressureStruct from '../../../src/domains/ArterialPressureDomain/IArterialPressureStruct';
import IArterialPressureCreatorModelFactory from '../../../src/models/ArterialPressureModel/IArterialPressureCreatorModelFactory';

class MockModel implements IArterialPressureCreatorDomain {
  private _arterialPressureStruct: IArterialPressureStruct;

  constructor(arterialPressureStruct: IArterialPressureStruct) {
    this._arterialPressureStruct = arterialPressureStruct;
  }
  public async create(): Promise<IArterialPressureStruct> {
    console.log('модель для тестирования');
    return this._arterialPressureStruct;
  }
}

class MockModelFactory implements IArterialPressureCreatorModelFactory {
  public getInstance(
    arterialPressureStruct: IArterialPressureStruct
  ): IArterialPressureCreatorDomain {
    return new MockModel(arterialPressureStruct);
  }
}

class MockModelError implements IArterialPressureCreatorDomain {
  private _arterialPressureStruct: IArterialPressureStruct;

  constructor(arterialPressureStruct: IArterialPressureStruct) {
    this._arterialPressureStruct = arterialPressureStruct;
  }
  public async create(): Promise<IArterialPressureStruct> {
    throw new Error('modelError');
    return this._arterialPressureStruct;
  }
}

class MockModelErrorFactory implements IArterialPressureCreatorModelFactory {
  public getInstance(
    arterialPressureStruct: IArterialPressureStruct
  ): IArterialPressureCreatorDomain {
    return new MockModelError(arterialPressureStruct);
  }
}

it('normal', async () => {
  const arterialPressureStruct: IArterialPressureStruct = {
    userId: '1211',
    sys: 1112,
    dia: 70,
    pulse: 85,
    timeCreate: new Date(),
  };
  const arterialPressure = new ArterialPressureCreatorDomain(
    arterialPressureStruct,
    new MockModelFactory()
  );
  const result = await arterialPressure.create();

  expect(result.sys).toEqual(arterialPressureStruct.sys);
  expect(result.dia).toEqual(arterialPressureStruct.dia);
  expect(result.pulse).toEqual(arterialPressureStruct.pulse);
});

it('DB error', async () => {
  const arterialPressureStruct: IArterialPressureStruct = {
    userId: 'userId',
    sys: 1112,
    dia: 70,
    pulse: 85,
    timeCreate: new Date(),
  };
  const arterialPressure = new ArterialPressureCreatorDomain(
    arterialPressureStruct,
    new MockModelErrorFactory()
  );
  try {
    const result = await arterialPressure.create();

    expect(result.sys).toEqual(arterialPressureStruct.sys);
    expect(result.dia).toEqual(arterialPressureStruct.dia);
    expect(result.pulse).toEqual(arterialPressureStruct.pulse);
  } catch (err) {
    if (err.message === 'modelError') {
      expect(true).toEqual(true);
    } else {
      expect(true).toEqual(false);
    }
  }
});
