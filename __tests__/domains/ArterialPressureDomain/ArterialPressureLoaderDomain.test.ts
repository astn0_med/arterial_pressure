import IArterialPressureStruct from '../../../src/domains/ArterialPressureDomain/IArterialPressureStruct';
import IArterialPressureLoaderModelFactory from '../../../src/models/ArterialPressureModel/IArterialPressureLoaderModelFactory';
import ArterialPressureLoaderDomain from '../../../src/domains/ArterialPressureDomain/ArterialPressureLoaderDomain';
import IArterialPressureLoaderDomain from '../../../src/domains/ArterialPressureDomain/IArterialPressureLoaderDomain';

class Model implements IArterialPressureLoaderDomain {
  private _userId: string;
  constructor(userId: string) {
    this._userId = userId;
  }
  public async loadAll(): Promise<Array<IArterialPressureStruct>> {
    const test: IArterialPressureStruct = {
      userId: this._userId,
      sys: 1112,
      dia: 70,
      pulse: 1,
      timeCreate: new Date(),
    };

    return [test, test, test, test];
  }
}

class ModelFabric implements IArterialPressureLoaderModelFactory {
  public getInstance(userId: string): IArterialPressureLoaderDomain {
    return new Model(userId);
  }
}

it('normal', async () => {
  const userId: string = '11111';

  const arterialPressureLoaderDomain = new ArterialPressureLoaderDomain(
    userId,
    new ModelFabric()
  );
  const result: Array<IArterialPressureStruct> = await arterialPressureLoaderDomain.loadAll();

  console.log('result = ', result);

  expect(result[0].userId).toEqual(userId);
});

// todo: добавь пагинацию
