import ArterialPressureCreatorDomainValidator from '../../../src/domains/ArterialPressureDomain/ArterialPressureCreatorDomainValidator';
import IArterialPressureStruct from '../../../src/domains/ArterialPressureDomain/IArterialPressureStruct';

it('validator ok', async () => {
  const arterialPressureStruct: IArterialPressureStruct = {
    userId: 'userId',
    sys: 1112,
    dia: 70,
    pulse: 85,
    timeCreate: new Date(),
  };
  const validator = new ArterialPressureCreatorDomainValidator(
    arterialPressureStruct
  );

  validator.check();

  expect(true).toEqual(true);
});

it('validator error ', async () => {
  const arterialPressureStructList: Array<any> = [
    {
      userId: 'userId',
      sys: ' ',
      dia: 70,
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: ' ',
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: 60,
      pulse: ' ',
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: null,
      dia: 70,
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: null,
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: 60,
      pulse: null,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: '12',
      dia: 70,
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: '12',
      pulse: 85,
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: 60,
      pulse: '12',
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: 60,
      pulse: {},
      timeCreate: new Date(),
    },
    {
      userId: 'userId',
      sys: 70,
      dia: 60,
      pulse: 50,
      timeCreate: 'r23r23r23r',
    },
    {
      userId: 12,
      sys: 70,
      dia: 60,
      pulse: 50,
      timeCreate: new Date(),
    },
    {
      userId: null,
      sys: 70,
      dia: 60,
      pulse: 50,
      timeCreate: new Date(),
    },
    {
      userId: {},
      sys: 70,
      dia: 60,
      pulse: 50,
      timeCreate: new Date(),
    },
  ];

  let noError: boolean = false;
  for (let items of arterialPressureStructList) {
    const validator = new ArterialPressureCreatorDomainValidator(items);

    try {
      validator.check();
      noError = true;
      break;
    } catch (err) {}
  }
  expect(noError).toEqual(false);
});
