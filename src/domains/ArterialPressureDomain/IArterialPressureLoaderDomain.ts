import IArterialPressureStruct from './IArterialPressureStruct';

export default interface IArterialPressureLoaderDomain {
  loadAll(): Promise<Array<IArterialPressureStruct>>;
}
