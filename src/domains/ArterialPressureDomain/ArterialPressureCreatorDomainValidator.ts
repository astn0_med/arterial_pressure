import IArterialPressureStruct from '../ArterialPressureDomain/IArterialPressureStruct';
import IValidator from '../Validators/IValidator';

export default class ArterialPressureCreatorDomainValidator
  implements IValidator {
  private readonly _arterialPressureStruct: IArterialPressureStruct;
  private readonly _errorMessage: string =
    'ArterialPressureCreatorDomainValidator error';
  private _validatorValues: Map<Function, string>;

  constructor(arterialPressureStruct: IArterialPressureStruct) {
    this._arterialPressureStruct = arterialPressureStruct;

    this._validatorValues = new Map();
    this._validatorValues.set(
      () => typeof this._arterialPressureStruct.sys !== 'number',
      this._errorMessage
    );
    this._validatorValues.set(
      () => typeof this._arterialPressureStruct.dia !== 'number',
      this._errorMessage
    );
    this._validatorValues.set(
      () => typeof this._arterialPressureStruct.pulse !== 'number',
      this._errorMessage
    );
    this._validatorValues.set(
      () => this._arterialPressureStruct.timeCreate instanceof Date === false,
      this._errorMessage
    );
    this._validatorValues.set(
      () => typeof this._arterialPressureStruct.userId !== 'string',
      this._errorMessage
    );
  }

  public check(): void {
    for (let checkFunc of this._validatorValues.keys()) {
      if (checkFunc()) {
        throw new Error(this._errorMessage);
      }
    }
  }
}
