import IArterialPressureStruct from "./IArterialPressureStruct";

export default interface IArterialPressureCreatorDomain {
   create(): Promise<IArterialPressureStruct>
}