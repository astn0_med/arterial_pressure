import IArterialPressureLoaderDomain from './IArterialPressureLoaderDomain';
import IArterialPressureStruct from './IArterialPressureStruct';
import IArterialPressureLoaderModel from '../../models/ArterialPressureModel/IArterialPressureLoaderModel';
import IArterialPressureLoaderModelFactory from '../../models/ArterialPressureModel/IArterialPressureLoaderModelFactory';

export default class ArterialPressureLoaderDomain
  implements IArterialPressureLoaderDomain {
  private _arterialPressureLoaderModelFactory: IArterialPressureLoaderModelFactory;
  private readonly _userId: string;

  constructor(
    userId: string,
    arterialPressureLoaderModelFactory: IArterialPressureLoaderModelFactory
  ) {
    this._userId = userId;
    this._arterialPressureLoaderModelFactory = arterialPressureLoaderModelFactory;
  }

  public async loadAll(): Promise<Array<IArterialPressureStruct>> {
    const arterialPressureLoaderModel: IArterialPressureLoaderModel = this._arterialPressureLoaderModelFactory.getInstance(
      this._userId
    );
    const result: Array<IArterialPressureStruct> = await arterialPressureLoaderModel.loadAll();
    return result;
  }
}
