export default interface IArterialPressureStruct {
  userId: string;
  sys: number;
  dia: number;
  pulse: number;
  timeCreate: Date;
}
