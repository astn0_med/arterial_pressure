import IArterialPressureCreatorModelFactory from '../../models/ArterialPressureModel/IArterialPressureCreatorModelFactory';
import IArterialPressureCreatorDomain from './IArterialPressureCreatorDomain';
import IArterialPressureStruct from './IArterialPressureStruct';

export default class ArterialPressureCreatorDomain
  implements IArterialPressureCreatorDomain {
  private _arterialPressureStruct: IArterialPressureStruct;
  private _ArterialPressureCreatorModelFactory: IArterialPressureCreatorModelFactory;

  constructor(
    arterialPressureStruct: IArterialPressureStruct,
    ArterialPressureCreatorModelFactory: IArterialPressureCreatorModelFactory
  ) {
    this._arterialPressureStruct = arterialPressureStruct;
    this._ArterialPressureCreatorModelFactory = ArterialPressureCreatorModelFactory;
  }

  public async create(): Promise<IArterialPressureStruct> {
    const arterialModel: IArterialPressureCreatorDomain = this._ArterialPressureCreatorModelFactory.getInstance(
      this._arterialPressureStruct
    );

    try {
      const result = await arterialModel.create();
      return result;
    } catch (err) {
      throw err;
    }
  }
}
