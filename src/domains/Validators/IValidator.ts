export default interface IValidator {
  check(): void;
}
