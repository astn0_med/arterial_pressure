import IArterialPressureCreatorDomain from '../../domains/ArterialPressureDomain/IArterialPressureCreatorDomain';
import IArterialPressureStruct from '../../domains/ArterialPressureDomain/IArterialPressureStruct';

export default interface IArterialPressureCreatorModelFactory {
  getInstance(
    arterialPressureStruct: IArterialPressureStruct
  ): IArterialPressureCreatorDomain;
}
