import IArterialPressureLoaderDomain from '../../domains/ArterialPressureDomain/IArterialPressureLoaderDomain';

export default interface IArterialPressureLoaderModelFactory {
  getInstance(userId: string): IArterialPressureLoaderDomain;
}
